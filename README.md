# Anthony Côté (Curriculum Vitae)

Ex-entrepreneur formé dans le domaine des affaires, qui s'est reconverti en développement de logiciel.

## Objectif

J’ai l’intention de devenir Développeur Back-end ou Full-Stack. Je souhaite me spécialiser dans les systèmes d’informations C.R.U.D.

## Formations

| Formation                    | Spécialisation        |Lieu|Dates|Détails|
|------------------------------|:---------------------:|:--:|:---:|-------|
| Techniques de l’Informatiques|Informatique de gestion| Cégep de Sherbrooke    | 2017-2020 | Dévelop. Logiciel, Web, Bases de données, Systèmes d'exploitation, Réseau|
| Techniques en Administration | Gestion de commerce   | Cégep de Trois-Rivières| 2009-2012 | Management, Marketing, Projets, Comptabilité, Leadership, Ventes, Logistique|

## Expériences

### Consultant(Contractuel pour Auticonsult depuis 2020

- Auto-formation

### Développeur PHP (Stage ATÉ pour InfoLogistique pendant l'Été 2019)

- Dévelopement de "Specsheet" à partir d’une BD existante pour le site e-commerce d'une multi-nationale.
- Traitement de données pour pouvoir en faire l’importation.
- Recherche et évaluation d’extensions, pour l’ajout de "features".

### Développeur Java (Stage ATÉ chez Globe Technologie pendant l'Été 2018)

- Créer un nouveau projet pour un API REST avec une base de données MySQL.
- Connecter des objets json avec les objets de la base de données.
- Gérer mon temps efficacement (télétravail).

### Programmeur VB (Emploi d'Été chez Globe Technologie pendant l'Été 2017)

- Reprendre en main une application non-maintenue (méthodes de 1200 lignes de code, etc).
- Créer de la documentation à partir de zéro.
- Débugger des exceptions non-gérées sans informations sur la cause.
- Gérer mon temps efficacement (télétravail).

### Entrepreneur/Co-propriétaire de Pourvoirie Oxygène (2012 – 2017)

- Gestion de l'entreprise : Planification/Administration, Service-Clientèle, Comptabilité/Finances, Marketing/Ventes, Logistique, etc.

## Projets

<center>

| Name              |      Type    |  LOC  |  Lang   |   |
|-------------------|:------------:|:-----:|--------:|---|
| Product. Modulaire| Cégep/Équipe | 400 h | TS/Node | Commandes de produits modulaires
| Proto             | Personnel    | ~2400 | Kotlin  | Logiciel de génération de code CRUD (Kotlin, SQL, Html) à partir d’un modèle logique et de spécifications de l’application.|
| Baseball RallyeCap| Cégep/Équipe | ~2000 | PHP/JS  | Analyse de besoins et conception de l’architecture logicielle d’intranet CRUD. |
| Minecraft2D       | Cégep/Équipe | ~2200 | C++/SFML| Jeu plateforme avec terrain destructible, lancement de projectiles à angles.|
| Scripts           | Personnel    | ~1900 | Python  | Divers scripts Python3 [FizzBuzz, Prime Finder, File Renamer, Automater, ...]|

## Compétences techniques

| Langage  | Niveau  |
|----------|:--------|
| Java     | ███████
| Kotlin   | ███████
| C#       | ███
| PHP      | █████
| HTML     | █████
| CSS      | ████████
|JavaScript| ████
| C++      | ██████
| Python   | ██████
| Bash/Zsh | ███
| Clojure  | ███
| Rust     | █
|||
| **Frameworks**||
| Spring     | ██████
| Android    | ██████
| SFML       | ██████
| Swing      | ███
| Asp.NET    | ██
| Pandas     | ██
| Android    | ██
|Node/Express| █
| Ktor       | █
|||
| **Role**||
| Back-end               | ██████
| Analyste d’affaires    | ██████
| Front-end              | ███
| DBA                    | ███
| Intelligence d’Affaires| ███
| Multimédia             | ██
| DevOps                 | ██
| Assurance Qualité      | ██
| Cyber sécurité         | ██
| UI/UX                  | ██
| Réseautique            | ██
|||
| **Ingénierie**||
| Organisation   | █████
| Design Pattern | █████
| Plant Uml      | █████
| Kanban         | ███
| Agile          | ██
|||
| **Paradigme**||
| P. Object Oriented  | █████
| P. Functionnel/LINQ | █████
| P. Déclarative      | ███
| P. Logique          | █
|||
| **Style de code**||
| Lisibilité        | ███████
| Réutilisable      | ██████
| Extensible        | █████
| Robuste           | █████
| Testable          | █████
| Sécure            | ███
| Optimization CPU  | ███
| Usabilité         | ██
|||
| **Base de données** ||
| MySQL      | ████
| SQL Server | ██
| SQLite     | █
|||
| **Système d’exploitation** ||
| Linux   | █████
| Windows | ████
| Mac     | █
|||
| **DevOps** ||
| Git Cli     | ██████
| Terminal    | ███
| Gitlab CI   | █
| Docker      | █
|||
| **Langues** ||
| Français     | █████
| Anglais      | ████
|||
| **Réflection**||
| Analyse profonde  | ███████
| Créativité        | █████
| Priorisation      | █████
| Pensée abstraite  | █████
| Estimation de temp| ████
| Décision rapide   | ██
|||
| **Compétences douces**
| Professionel      | ██████
| Initiative        | █████
| Auto-didacte      | █████
| Leadership        | ███
| Enseignement      | ███
| Style             | ███
| Chaleureux        | ██
| Socialisation     | ██
| Gestion du stress | █
| Verbosité         | █
|||
| **Affaires**
| Marketing           | ████
| Comptabilité        | ███
| Logistique          | ███
| Finances            | ██
| Ventes              | ██
| Ressources humaines | ██
|||
| **Industries**
| Foresterie           | █████
| Hôtellerie           | ███
| Construction         | ███
| Alimentaire          | ██
| Transport Industriel | ██
| Mécanique            | ██
|||
| **IDE** |
| Suite JetBrains| ██████
| Visual Studio  | █████
| VS Code        | ████
| Vim            | ███

</center>

## Autres

- Gagnant du **«Prix Mérite 2012»** en Gestion de commerces du Cégep de Trois-Rivières
- Sur le spectre de l'autisme & trouble de dépersonnalisation
- Intérêt pour les systèmes d’informations (CRUD, ERP, …)
- Lecture de plus de 750 livres
  - Affaires
  - Technologie
  - Développement personnel

## Contact

email = "CoteA" + "@" + "protonmail.com"

*Last update : 2020-08-05*
